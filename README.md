# My LaTeX Template

## Usage

1. Copy the template
2. Change the headers in ``/headers.tex``
3. Write accronyms in ``/report.tex``
4. Write your abstract, body, etc. in ``/body.tex``
5. Write the sources in ``/report.bib``

# About

- Author: Arthur Poulet
- Licence: CC-0
